import datetime
import os
import subprocess

import mazdap as mz
import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine
from sqlalchemy.types import Boolean, DateTime, Float, Integer, String

import credentials

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV"
    # ,
    # max_identifier_length=128,
)

# Update to current environments rclone name to sync properly
rclone_location = "onedrive_operational_strategy"

def get_cv_records():
    cv_query = """SELECT
        VIN,
        CV_VEHICLE_FLAG,
        CV_ACTIVATION_STATUS,
        CV_ENROLLMENT_DATE,
        MYMAZDA_PHNO
    FROM
        (
        SELECT
            DISTINCT a.asset_num "VIN",
            a.X_CONN_VEH_FLG "CV_VEHICLE_FLAG" ,
            d.X_CV_ACTIVATION_STATUS "CV_ACTIVATION_STATUS",
            to_char(d.X_CV_TC_TIME, 'MM/DD/YYYY HH24:MI:SS') "CV_ENROLLMENT_DATE",
            conx.ATTRIB_41 "MYMAZDA_PHNO"
        FROM
            edw_stg.sbl_asset a
        INNER JOIN edw_stg.sbl_asset_con d ON
            a.row_id = d.asset_id
            AND (d.X_CV_ROLE IS NULL
            OR d.X_CV_ROLE = 'Primary Driver')
        LEFT OUTER JOIN edw_stg.sbl_contact con ON
            con.row_id = d.contact_id
        LEFT OUTER JOIN edw_stg.sbl_contact_x conx ON
            conx.par_row_id = con.par_row_id
        WHERE
            a.X_CONN_VEH_FLG = 'Y'
            AND a.bu_id = '1-XO3')""".strip()

    cv_df = pd.read_sql(cv_query, engine)
    cv_df["cv_enrollment_date"] = pd.to_datetime(cv_df["cv_enrollment_date"])
    cv_df = cv_df.sort_values("cv_enrollment_date", ascending=False).drop_duplicates(
        "vin", keep="first"
    )
    cv_df["mymazda_phno"] = cv_df["mymazda_phno"].str.replace(r"[^0-9]+", "")
    return cv_df

def get_cv_records_manual():
    cv_df = pd.read_csv("cm_cv_reg.csv", sep="|")
    cv_df.columns = map(str.lower, cv_df.columns)
    cv_df["cv_enrollment_date"] = pd.to_datetime(cv_df["cv_enrollment_date"])
    cv_df = cv_df.sort_values("cv_enrollment_date", ascending=False).drop_duplicates(
        "vin", keep="first"
    )
    cv_df["mymazda_phno"] = cv_df['mymazda_phno'].astype(str).str.extract('(\d+)')
    return cv_df
    

def get_sls_records():
    reportpath = "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Vehicle Sales - By Dealer (MyMazda)"

    conn = mz.ObieeConnector(uid=credentials.uid, pwd=credentials.pwd)
    rtl_sls_emp = conn.get_csv(reportpath)

    rtl_sls_emp["District Code"] = rtl_sls_emp["District Code"].astype(str)
    rtl_sls_emp["Calendar Date"] = pd.to_datetime(rtl_sls_emp["Calendar Date"])
    rtl_sls_emp["Pure Retail"] = rtl_sls_emp["Pure Retail"].astype(int)
    rtl_sls_emp["CPO Vehicle Sales"] = rtl_sls_emp["CPO Vehicle Sales"].astype(int)

    ## remove cpo sales
    rtl_sls_emp = rtl_sls_emp[rtl_sls_emp["CPO Vehicle Sales"] == 0]

    ## dedup
    true_vins = rtl_sls_emp.groupby("Vin")["Pure Retail"].sum().reset_index()

    rtl_sls_emp_cleaned = (
        rtl_sls_emp.sort_values("Calendar Date", ascending=False)
        .drop_duplicates("Vin", keep="first")
        .drop("Pure Retail", axis=1)
        .merge(true_vins, on="Vin")
    )
    return rtl_sls_emp_cleaned


def get_emp_data():
    emp_master = pd.read_sql(
        "SELECT PRSN_ID_CD, FRST_NM || ' ' || LAST_NM as EMP_NAME FROM EDW_STG.BTC03020_DLR_EMPL ORDER BY JOB_START_DT DESC",
        engine,
    )
    emp_master = emp_master.drop_duplicates(subset=["prsn_id_cd"])
    return emp_master

def get_dim_date():
    ## date master
    today = datetime.date.today()
    last_sunday = today - datetime.timedelta(days=today.weekday() + 1)
    date_master = pd.DataFrame(
        {"cal_date": pd.date_range(start="10/01/2019", end=last_sunday)}
    )
    cal_master = pd.read_sql(
        "SELECT cal_date, sales_yr, sales_mo, sales_mo_name_yr FROM RPR_STG.KHN_CALENDAR_MASTER",
        engine,
    )
    date_master = date_master.merge(cal_master, how="left")
    date_master.to_csv("data/date_master.csv", index=False)


cv_df = get_cv_records_manual()
# cv_df = get_cv_records()

sls_df = get_sls_records()

emp_df = get_emp_data()


cv_compatible_cars = [
    "C30 2021",
    "C30 2020",
    "CX9 2021",
    "CX5 2021",
    "M3S 2021",
    "M3H 2021",
    "M3S 2020",
    "M3H 2020",
    "M3S 2019",
    "M3H 2019",
    "M30 2022",
    "CX9 2022",
    "CX5 2022",
    "M3S 2022",
    "M3H 2022",
]
sls_df["CV_VEHICLE_FLAG"] = (sls_df["Carline"] + " " + sls_df["Model Year"]).isin(
    cv_compatible_cars
)

fraud_df = sls_df.query(
    "`Pure Retail` == 1 & CV_VEHICLE_FLAG == True"
).dropna(axis=0, subset=["Mda Cd"])


fraud_df = fraud_df.merge(cv_df, left_on=["Vin"], right_on=["vin"], how="left").merge(
    emp_df, left_on=["Retl Sls Emp Cd"], right_on=["prsn_id_cd"], how="left"
)

fraud_df.to_csv("data/fraud_master.csv", index=False)

## write dim_date
get_dim_date()

sync_cmd = f"rclone sync -v data {rclone_location}:wphyo/cv_fraud"
with open("logging.txt", "w") as f:
    process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
    while True:
        return_code = process.poll()
        if return_code is not None:
            break
